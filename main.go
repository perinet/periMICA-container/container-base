/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package main

import (
	"encoding/json"
	"log"

	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/utils/intHttp"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/apiservice/staticfiles"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
	"gitlab.com/perinet/periMICA-container/apiservice/ssh"

	"gitlab.com/perinet/generic/lib/httpserver"
)

func init() {
	log.SetPrefix("Service BaseContainer: ")
	log.Println("Starting")

	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "dns-sd", "ssh"}
	data, _ := json.Marshal(services)
	intHttp.Put(node.ServicesSet, data, nil)

	node.RegisterNodeEventCallback(setDnssdAdv)

	security.RegisterSecurityEventCallback(func() {
		setHttpSecurity()
		httpserver.Restart()
	})
}

func setDnssdAdv() {
	var data []byte
	var err error

	data = intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	data = intHttp.Get(node.ProductionInfoGet, nil)
	var productionInfo node.ProductionInfo
	err = json.Unmarshal(data, &productionInfo)
	if err != nil {
		log.Println("Failed to fetch ProductionInfo: ", err.Error())
	}

	//start advertising _https via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{Port: 443}
	dnssdServiceInfo.TxtRecord = dnssd.DNSSDTxtRecordHttps{Format: "uAPI", ApiVersion: nodeInfo.ApiVersion, ApplicationName: nodeInfo.Config.ApplicationName, ElementName: nodeInfo.Config.ElementName, ProductName: productionInfo.ProductName}
	data, _ = json.Marshal(dnssdServiceInfo)
	intHttp.Patch(dnssd.DNSSDServiceInfoAdvertiseSet, data, map[string]string{"service_name": "_https._tcp"})

	//start advertising _ssh via dnssd
	dnssdServiceInfo = dnssd.DNSSDServiceInfo{Port: 22}
	dnssdServiceInfo.TxtRecord = dnssd.DNSSDTxtRecordHttps{Format: "uAPI", ApiVersion: nodeInfo.ApiVersion, ApplicationName: nodeInfo.Config.ApplicationName, ElementName: nodeInfo.Config.ElementName, ProductName: productionInfo.ProductName}
	data, _ = json.Marshal(dnssdServiceInfo)
	intHttp.Patch(dnssd.DNSSDServiceInfoAdvertiseSet, data, map[string]string{"service_name": "_ssh._tcp"})
}

func setHttpSecurity() {
	var securityConfig security.SecurityConfig
	err := json.Unmarshal(intHttp.Get(security.Security_Config_Get, nil), &securityConfig)
	if err != nil {
		log.Println("error getting security config: ", err)
		return
	}

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      intHttp.Get(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    intHttp.Get(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: intHttp.Get(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})
}

func main() {
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(ssh.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())

	setDnssdAdv()
	setHttpSecurity()

	httpserver.HttpRedirect("[::]:80", 443)
	httpserver.ListenAndServe("[::]:443")
}
