module gitlab.com/perinet/periMICA-container/service/base

go 1.22.0

require (
	gitlab.com/perinet/generic/apiservice/dnssd v1.0.0
	gitlab.com/perinet/generic/apiservice/staticfiles v1.0.0
	gitlab.com/perinet/generic/lib/httpserver v1.0.1
	gitlab.com/perinet/generic/lib/utils v1.0.1
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v1.0.2
	gitlab.com/perinet/periMICA-container/apiservice/network v1.0.0
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.7
	gitlab.com/perinet/periMICA-container/apiservice/security v1.0.3
	gitlab.com/perinet/periMICA-container/apiservice/ssh v1.0.0
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.2-0.20240210093433-b8dc0fc11e7e // indirect
	golang.org/x/exp v0.0.0-20241108190413-2d47ceb2692f // indirect
)
